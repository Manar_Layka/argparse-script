import argparse
import re


'''
The input of this script will be like:
python3 task1.py "www.example.com" --vars foo=bar x=6
and The output will be:
Namespace(arg2=' ', arg3=' ', domain='www.example.com', vars=['foo=bar', 'x=6'], verbose=False)
{'foo': 'bar', 'x': '6'}
'''


def parse_var(s):
    """
    Parse a key, value pair, separated by '='
    That's the reverse of ShellArgs.

    On the command line (argparse) a declaration will typically look like:
        foo=hello
    or
        foo="hello world"
    """
    items = s.split('=')
    key = items[0].strip() # we remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
    return (key, value)


def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
            # d[key].append(value)
    return d


def main():
    # try:
    #     import validators
    # except ModuleNotFoundError:
    #     print('Please pip install validators')

    parser = argparse.ArgumentParser(description="This is to demonstrate multiple prefix characters")
    parser.add_argument("domain", type=str,
                        help='validate a domain to be like example.com')
    parser.add_argument("-arg2", default=" ", required=False, help="optional argument")
    parser.add_argument("-arg3", default=" ", required=False, help="optional argument")
    # parser.add_argument('key', help="foo element")
    # parser.add_argument('value', nargs='*', help="bar element list")
    parser.add_argument('--vars', nargs='*', help="This is a list of dictionaries")
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='an optional argument')
    domain = parser.parse_args().domain
    # if validators.domain(
    #         domain):
    if re.match(r"\b((?=[a-z0-9-]{1,63}\.)[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b", domain):
        args = parser.parse_args()
        values = parse_vars(args.vars)
        print(args)
        print(values)
        # your code goes here


if __name__ == '__main__':
    main()

